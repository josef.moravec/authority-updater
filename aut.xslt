<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    version="1.0">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/> 
    <xsl:strip-space elements="*"/> 
    <xsl:template match="/*">
        <xsl:copy>
            <xsl:copy-of select="@* | //namespace::* | child::node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>

