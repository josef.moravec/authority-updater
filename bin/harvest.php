<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use VuFindHarvest\OaiPmh\HarvesterCommand;


$command = new HarvesterCommand();
$application = new Application();
$application->add($command);
$application->setDefaultCommand($command->getName(), true);
return $application->run();
