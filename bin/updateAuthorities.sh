#!/bin/bash

# include helper functions
if [ -f "/usr/share/koha/bin/koha-functions.sh" ]; then
    . "/usr/share/koha/bin/koha-functions.sh"
else
    echo "Error: /usr/share/koha/bin/koha-functions.sh not present." 1>&2
    exit 1
fi

DIRNAME=$(dirname $0);

# Include config file
if [ -f "$DIRNAME/../config/config.sh" ]; then
    . "$DIRNAME/../config/config.sh"
else
    echo "Error: $DIRNAME/../config/config.sh not present." 1>&2
    exit 1
fi

###  Functions

## Usage function
usage()
{
    local scriptname=$(basename $0)

    cat <<EOF
$scriptname
This script update authority records based on updates in National library Prague authority file
Usage:
$scriptname instancename1 [instancename2]
$scriptname -h|--help
    --help|-h             Display this help message
EOF
}

## Stage function
stage()
{
    local instance="$1"
    local file="$2"
    local match_rule="$3"
    local result=$(koha-shell $instance -c "perl /usr/share/koha/bin/stage_file.pl --file $file --authorities --format MARCXML --match $match_rule --no-create")
    local batch_id=$(echo "$result" | sed -n "s/.*Batch number assigned:\s*\([0-9]*\).*/\1/p")
    echo "$batch_id"
}

## Import function
import()
{
    local instance="$1"
    local batch="$2"
    koha-shell $instance -c "perl /usr/share/koha/bin/commit_file.pl --batch-number $batch"
}

clean()
{
    local instance="$1"
    local batch="$2"
    echo "DELETE FROM import_batches WHERE import_batch_id=$batch" | koha-mysql "$instance"
}

## Read command line parameters
while [ $# -gt 0 ]; do

    case "$1" in
        -h|--help)
            usage ; exit 0 ;;
        -*)
            die "Error: invalid option switch ($1)" ;;
        *)
            # We expect the remaining stuff are the instance names
            break ;;
    esac

done


DIR=$(cd `dirname $0` && pwd)

# Harvest records

cd $DIR/..

# Date of first Koha installation in Czech Republic
php $DIR/harvest.php --ini=aut.ini --from="2015-01-01"

# Transform records
mkdir "AUT/transformed/"

FILES=AUT/*.xml

for file in AUT/*.xml
do
    filename=$(basename $file)
    if [ -e "$filename" ]; then
	xsltproc aut.xslt $file > "AUT/transformed/$filename"
	rm $file
    fi
done

# Loop through instances
if [ $# -gt 0 ]; then
    # We have at least one instance name
    for name in "$@"; do

        if is_instance $name; then
            for file in AUT/transformed/*.xml
            do
                # Stage records into batch
                batch_id=$(stage $name $file $MATCH_RULE)

                # Import batch
                import $name $batch_id

                # Remove imported batches
                clean $name $batch_id

                # Delete file
                rm $file
            done

        else
            echo "Error: Invalid instance name $name"
        fi

    done
else
    echo "Error: you must provide at least one instance name"
fi

# Remove downloaded files

