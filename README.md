# Authority updater

## Prerequisities

1. Koha server
2. With PHP installed
3. Indexed $7 of authority heading in Zebra

## Installation

In command line of your Koha server run:

    git clone https://gitlab.com/josef.moravec/authority-updater.git
    cd authority-updater
    composer install

## Configuration

Insert a proper match rule to your Koha instance. You could use file `sql/match_rule.sql` and run it on top of your Koha instance database.

    cp config/config.sh.dist config/config.sh

If you created your match rule using provided sql file, you are done, otherwise you need to set corresponding match rule id in config.sh

## Usage

Harvest authorities from national library:

    php bin/harvest.php --ini=aut.ini AUT

For more details about harvester, go to https://github.com/vufind-org/vufindharvest 

Update your authorities:

    bin/updateAuthorities.sh your_koha_instance
